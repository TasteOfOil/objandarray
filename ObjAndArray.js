console.log("Arrays: ");
arr1 = ["Вася", "Петя", "Жанна"];
arr2 = [3,5,2,7];

console.log(arr1);
arr1.push("Аня");
arr1.unshift("Аня");
console.log(arr1);
arr1.pop();
console.log(arr1);
arr1.sort();
console.log(arr1);
arr1.splice(0,1);
console.log(arr1);
arr1.reverse();
console.log(arr1);


console.log("\n"+arr2);
arr2[4] = 44;
console.log(arr2);

var str = "Ребята , давайте жить дружно !";
var arr3 = str.split(" ");
console.log("\n"+str);
console.log(arr3);

var str2 = arr3.join("|");
console.log(str2);


console.log("\nObjects:");
function Person(_name, _age, _id){
    this.name = _name;
    this.age = _age;
    this.id = _id;
    return this;
}

var p1 = new Person("Сеня", 14, 100);
var p2 = new Person("Костя", 19, 200);
var p3 = new Person("Ульяна", 22, 300);
var p4 = new Person("Василиса", 10, 400);
console.log(p1);
console.log(p2);
console.log(p3);
console.log(p4);

var arrPers = [p1,p2,p3,p4];
console.log("\nArray of Objects: "+ arrPers);

function printPers(item, index, array){
    console.log(item);
}
console.log("\nforEach:");
arrPers.forEach(printPers);

console.log("\nfind:");
var res1 = arrPers.find(item =>{return item.id==300;});
console.log(res1);
var res1Id = arrPers.findIndex(item =>{return item.id==300;});
console.log("Index: "+res1Id);

console.log("\nsort:");
arrPers.sort((a,b)=>{return b.age - a.age;});
console.log(arrPers);

console.log("\nfilter:");
var res4 = arrPers.filter(item => {return item.age <= 20});
console.log(res1);